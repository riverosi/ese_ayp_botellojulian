# ESE_AyP_BotelloJulian

Arquitectura y Programación de Sistemas Embebidos - FIUNER - 2018

Autor: Botello, Julián <jlnbotello@gmail.com>

Diseño de drivers de dispositivos

- MAX7219

- NRF24L01

Aplicación: Sistema de turnos inalámbrico

Documentación (Doxygen): intro_ciaa-master/doc/html/index.html
