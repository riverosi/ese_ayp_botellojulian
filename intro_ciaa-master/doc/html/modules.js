var modules =
[
    [ "CIAA Firmware", "group___c_i_a_a___firmware.html", "group___c_i_a_a___firmware" ],
    [ "GPIO HAL", "group__gpio__hal.html", "group__gpio__hal" ],
    [ "Leds", "group__leds.html", "group__leds" ],
    [ "Led Matrix Utils", "group__led__mat.html", "group__led__mat" ],
    [ "MAX7219", "group__max7219.html", "group__max7219" ],
    [ "NRF24L01", "group___n_r_f24_l01.html", "group___n_r_f24_l01" ],
    [ "SPI generic device", "group__spi__generic__device.html", "group__spi__generic__device" ],
    [ "SPI master HAL", "group__spi__master__hal.html", "group__spi__master__hal" ],
    [ "Switches", "group__switches.html", "group__switches" ],
    [ "UART debugging", "group__debug__uart.html", "group__debug__uart" ]
];