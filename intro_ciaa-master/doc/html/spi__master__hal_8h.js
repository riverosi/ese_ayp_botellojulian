var spi__master__hal_8h =
[
    [ "spiClockMode_t", "group__spi__master__hal.html#gaedec30f3ece57338dca835603aa676d3", [
      [ "SPI_CLK_MODE0", "group__spi__master__hal.html#ggaedec30f3ece57338dca835603aa676d3a067b0322b09ac4d3cc14bbf2995a8100", null ],
      [ "SPI_CLK_MODE1", "group__spi__master__hal.html#ggaedec30f3ece57338dca835603aa676d3a3680a418c288817b3eb426e5b5e8ae31", null ],
      [ "SPI_CLK_MODE2", "group__spi__master__hal.html#ggaedec30f3ece57338dca835603aa676d3ac0d2a9655586fb948b9fd9241810a691", null ],
      [ "SPI_CLK_MODE3", "group__spi__master__hal.html#ggaedec30f3ece57338dca835603aa676d3a1df2995a001f248aada529c305ae14b7", null ]
    ] ],
    [ "spiFrameBits_t", "group__spi__master__hal.html#gab9b451569dd68389a74ba69dd117b90d", [
      [ "SPI_BITS8", "group__spi__master__hal.html#ggab9b451569dd68389a74ba69dd117b90da10bd39ac136b1c1b579663ac1d06c382", null ],
      [ "SPI_BITS16", "group__spi__master__hal.html#ggab9b451569dd68389a74ba69dd117b90da5c0bfb13271d3c8cfdb4cd37d4fe651f", null ]
    ] ],
    [ "spiSsp_t", "group__spi__master__hal.html#ga279b37610b2c3c0606c6f7a19c61d0d6", [
      [ "SSP_0", "group__spi__master__hal.html#gga279b37610b2c3c0606c6f7a19c61d0d6a82824bff0ac36361f859d0dcea8b86be", null ],
      [ "SSP_1", "group__spi__master__hal.html#gga279b37610b2c3c0606c6f7a19c61d0d6a90e73f823d2f1ec469a4662d743ec1f3", null ]
    ] ],
    [ "SpiConfig", "group__spi__master__hal.html#gaf259cdbd1a09099217c9884abed51b11", null ],
    [ "SpiDeInit", "group__spi__master__hal.html#ga7c1ba864e3e51caa3c9ca168359751f0", null ],
    [ "SpiInit", "group__spi__master__hal.html#gaf5908b2588fbfad3a6c0cf6690c7211c", null ],
    [ "SpiReadBlocking", "group__spi__master__hal.html#gad83fa898adc7de23bb8dc5223d7d6a89", null ],
    [ "SpiRWBlocking", "group__spi__master__hal.html#gaa15b15b3168e1e3e8592b089df9edb77", null ],
    [ "SpiTestLoopBack", "group__spi__master__hal.html#ga9e9dc6d104c30c6d1ab6e6dbc744b80b", null ],
    [ "SpiWriteBlocking", "group__spi__master__hal.html#ga5a97628b47a87690f7654243e3a98f6b", null ]
];