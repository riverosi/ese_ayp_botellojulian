var group__spi__master__hal =
[
    [ "spiConfig_t", "structspi_config__t.html", [
      [ "bits", "structspi_config__t.html#af565714e8020f23ff1178041c3f41c27", null ],
      [ "clock_freq", "structspi_config__t.html#a456fb9ec3c621b0f79a86652202b3b25", null ],
      [ "clock_mode", "structspi_config__t.html#ab93d2bd987b4d6d6623f0afd9b19e48c", null ],
      [ "ssp", "structspi_config__t.html#a87f2f5225e4a6e263cf789b962a2a4b4", null ]
    ] ],
    [ "sspCfg_t", "structssp_cfg__t.html", [
      [ "func", "group__spi__master__hal.html#gacc21302b1f7302f29e9d14ef395d15a8", null ],
      [ "pin", "group__spi__master__hal.html#gab40a673fb19c1e650e1f79de91788aa5", null ],
      [ "port", "group__spi__master__hal.html#ga2fa54f9024782843172506fadbee2ac8", null ]
    ] ],
    [ "N_CONFIG", "group__spi__master__hal.html#ga668349dc8bf9c98d13011959b0311e83", null ],
    [ "spiClockMode_t", "group__spi__master__hal.html#gaedec30f3ece57338dca835603aa676d3", [
      [ "SPI_CLK_MODE0", "group__spi__master__hal.html#ggaedec30f3ece57338dca835603aa676d3a067b0322b09ac4d3cc14bbf2995a8100", null ],
      [ "SPI_CLK_MODE1", "group__spi__master__hal.html#ggaedec30f3ece57338dca835603aa676d3a3680a418c288817b3eb426e5b5e8ae31", null ],
      [ "SPI_CLK_MODE2", "group__spi__master__hal.html#ggaedec30f3ece57338dca835603aa676d3ac0d2a9655586fb948b9fd9241810a691", null ],
      [ "SPI_CLK_MODE3", "group__spi__master__hal.html#ggaedec30f3ece57338dca835603aa676d3a1df2995a001f248aada529c305ae14b7", null ]
    ] ],
    [ "spiFrameBits_t", "group__spi__master__hal.html#gab9b451569dd68389a74ba69dd117b90d", [
      [ "SPI_BITS8", "group__spi__master__hal.html#ggab9b451569dd68389a74ba69dd117b90da10bd39ac136b1c1b579663ac1d06c382", null ],
      [ "SPI_BITS16", "group__spi__master__hal.html#ggab9b451569dd68389a74ba69dd117b90da5c0bfb13271d3c8cfdb4cd37d4fe651f", null ]
    ] ],
    [ "spiSsp_t", "group__spi__master__hal.html#ga279b37610b2c3c0606c6f7a19c61d0d6", [
      [ "SSP_0", "group__spi__master__hal.html#gga279b37610b2c3c0606c6f7a19c61d0d6a82824bff0ac36361f859d0dcea8b86be", null ],
      [ "SSP_1", "group__spi__master__hal.html#gga279b37610b2c3c0606c6f7a19c61d0d6a90e73f823d2f1ec469a4662d743ec1f3", null ]
    ] ],
    [ "SpiConfig", "group__spi__master__hal.html#gaf259cdbd1a09099217c9884abed51b11", null ],
    [ "SpiDeInit", "group__spi__master__hal.html#ga7c1ba864e3e51caa3c9ca168359751f0", null ],
    [ "SpiInit", "group__spi__master__hal.html#gaf5908b2588fbfad3a6c0cf6690c7211c", null ],
    [ "SpiReadBlocking", "group__spi__master__hal.html#gad83fa898adc7de23bb8dc5223d7d6a89", null ],
    [ "SpiRWBlocking", "group__spi__master__hal.html#gaa15b15b3168e1e3e8592b089df9edb77", null ],
    [ "SpiTestLoopBack", "group__spi__master__hal.html#ga9e9dc6d104c30c6d1ab6e6dbc744b80b", null ],
    [ "SpiWriteBlocking", "group__spi__master__hal.html#ga5a97628b47a87690f7654243e3a98f6b", null ],
    [ "bits", "group__spi__master__hal.html#ga2e43b962eb433953ce837a33930d4590", null ],
    [ "clock_mode", "group__spi__master__hal.html#ga992f05ee30e4e4169f4af9adf658ff57", null ],
    [ "func", "group__spi__master__hal.html#gacc21302b1f7302f29e9d14ef395d15a8", null ],
    [ "initialized_ssp", "group__spi__master__hal.html#gab0ac776ad27d367a0ce5fd78854ca09b", null ],
    [ "pin", "group__spi__master__hal.html#gab40a673fb19c1e650e1f79de91788aa5", null ],
    [ "port", "group__spi__master__hal.html#ga2fa54f9024782843172506fadbee2ac8", null ],
    [ "ssp", "group__spi__master__hal.html#gae153c50568c2937b883f2fd9de1c385a", null ],
    [ "ssp_1_cfg", "group__spi__master__hal.html#gabc493371d223b2b6b410e281a7b60086", null ],
    [ "ssp_cfg", "group__spi__master__hal.html#gabbef5cf3f30628b5f9d6bbd64b7f82a4", null ]
];