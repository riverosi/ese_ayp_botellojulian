var spi__master__hal_8c =
[
    [ "N_CONFIG", "group__spi__master__hal.html#ga668349dc8bf9c98d13011959b0311e83", null ],
    [ "SpiConfig", "group__spi__master__hal.html#gaf259cdbd1a09099217c9884abed51b11", null ],
    [ "SpiDeInit", "group__spi__master__hal.html#ga7c1ba864e3e51caa3c9ca168359751f0", null ],
    [ "SpiInit", "group__spi__master__hal.html#gaf5908b2588fbfad3a6c0cf6690c7211c", null ],
    [ "SpiReadBlocking", "group__spi__master__hal.html#gad83fa898adc7de23bb8dc5223d7d6a89", null ],
    [ "SpiRWBlocking", "group__spi__master__hal.html#gaa15b15b3168e1e3e8592b089df9edb77", null ],
    [ "SpiTestLoopBack", "group__spi__master__hal.html#ga9e9dc6d104c30c6d1ab6e6dbc744b80b", null ],
    [ "SpiWriteBlocking", "group__spi__master__hal.html#ga5a97628b47a87690f7654243e3a98f6b", null ],
    [ "bits", "group__spi__master__hal.html#ga2e43b962eb433953ce837a33930d4590", null ],
    [ "clock_mode", "group__spi__master__hal.html#ga992f05ee30e4e4169f4af9adf658ff57", null ],
    [ "initialized_ssp", "group__spi__master__hal.html#gab0ac776ad27d367a0ce5fd78854ca09b", null ],
    [ "ssp", "group__spi__master__hal.html#gae153c50568c2937b883f2fd9de1c385a", null ],
    [ "ssp_1_cfg", "group__spi__master__hal.html#gabc493371d223b2b6b410e281a7b60086", null ],
    [ "ssp_cfg", "group__spi__master__hal.html#gabbef5cf3f30628b5f9d6bbd64b7f82a4", null ]
];