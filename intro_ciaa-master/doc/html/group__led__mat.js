var group__led__mat =
[
    [ "stagedPoint_t", "structstaged_point__t.html", [
      [ "staged", "structstaged_point__t.html#ac88754e6c216c0fc81f65fea928e15b7", null ],
      [ "status", "structstaged_point__t.html#ab73bed1b4a9109d214c0752bbdd149f5", null ],
      [ "x", "structstaged_point__t.html#a0f561e77fa0f040b637f4e04f6cd8078", null ],
      [ "y", "structstaged_point__t.html#a17f97f62d93bc8cfb4a2b5d273a2aa72", null ]
    ] ],
    [ "ledMatrix_t", "structled_matrix__t.html", [
      [ "dev", "structled_matrix__t.html#a89342f01f981d7250eb77a8661a02888", null ],
      [ "ori", "structled_matrix__t.html#a868e781b581803199b093f77a9c85fba", null ],
      [ "st_pt", "structled_matrix__t.html#a53d5dd8e731539b8d08805811ab503df", null ]
    ] ],
    [ "delta_t", "structdelta__t.html", [
      [ "dx", "structdelta__t.html#a6b061c378131aa307e80484d3165a27c", null ],
      [ "dy", "structdelta__t.html#ad2d492133432601fc8a5c11b7602759e", null ]
    ] ],
    [ "MATRIX_SIZE", "group__led__mat.html#ga2bd32349fcbeb6ee86434a65226cba1a", null ],
    [ "matrixOrientation_t", "group__led__mat.html#ga8fb070c465918a95ee2d3b296fe65e74", [
      [ "ROT_0_CW", "group__led__mat.html#gga8fb070c465918a95ee2d3b296fe65e74a9d77c4964b97af8590d8c7cd86be1a5f", null ],
      [ "ROT_90_CW", "group__led__mat.html#gga8fb070c465918a95ee2d3b296fe65e74ab0ff4aa7e9875eb64a828407d29b5716", null ],
      [ "ROT_180_CW", "group__led__mat.html#gga8fb070c465918a95ee2d3b296fe65e74ac50c1e5c993750be5c8d518a48e9e1cf", null ],
      [ "ROT_270_CW", "group__led__mat.html#gga8fb070c465918a95ee2d3b296fe65e74a1326c3ce26fb0b9835998d315cb5592c", null ]
    ] ],
    [ "moveDir_t", "group__led__mat.html#ga42e118908eddedc60d4b6f9e713f9a12", [
      [ "X_PLUS_1", "group__led__mat.html#gga42e118908eddedc60d4b6f9e713f9a12a8b658a435001622ba70a14c1d91f3186", null ],
      [ "X_MINUS_1", "group__led__mat.html#gga42e118908eddedc60d4b6f9e713f9a12a527f59e15a0515389d4b45423455a165", null ],
      [ "Y_PLUS_1", "group__led__mat.html#gga42e118908eddedc60d4b6f9e713f9a12a1a012ae8dd554c6c2a3378e0698ee554", null ],
      [ "Y_MINUS_1", "group__led__mat.html#gga42e118908eddedc60d4b6f9e713f9a12a9f4f5e49b36f98919ef2ac407b7ef2b8", null ]
    ] ],
    [ "MatrixBlank", "group__led__mat.html#gacc7f8d027e38813980af8bf134b9e24d", null ],
    [ "MatrixGetImage", "group__led__mat.html#gaa5618e59acffe7f04d4168e560c75835", null ],
    [ "MatrixGetPoint", "group__led__mat.html#gab2e9461cb0e3509c155ec3192ea5232d", null ],
    [ "MatrixInit", "group__led__mat.html#ga710c4a81ac95f201bcb246ec77bf75e7", null ],
    [ "MatrixMoveStagedPoint", "group__led__mat.html#gaebd46eae4b0cca4406e913d69e8ca9a9", null ],
    [ "MatrixOnTickUpdateSP", "group__led__mat.html#ga9c951795b21797dcb03bb0344e44ea4c", null ],
    [ "MatrixRotate", "group__led__mat.html#gae8350cc8eac7b6dd3c8ca49bb7a34c35", null ],
    [ "MatrixRstPoint", "group__led__mat.html#gae8cf6273cf81467a7f178274c17274ad", null ],
    [ "MatrixSetImage", "group__led__mat.html#gadafcc6eec23e941ba9549d83bcbac6a4", null ],
    [ "MatrixSetPoint", "group__led__mat.html#gae5a78e5cb7bd39f23c6d37f0bb4c4283", null ],
    [ "MatrixStagePoint", "group__led__mat.html#ga7da8758105d08e9fdeb35ee2dfedc418", null ],
    [ "MatrixTick", "group__led__mat.html#ga9b101c2492f0472e9bc8d6897eafae00", null ],
    [ "MatrixTogPoint", "group__led__mat.html#gaa9ae3dad7a9afa20577e75b77e6bec1a", null ],
    [ "MatrixTogStagePoint", "group__led__mat.html#ga6e8b5f951e0d82ebc6127f40fa18a1fc", null ],
    [ "MatrixUnstagePoint", "group__led__mat.html#ga640f9271eda47539d7ff7742715c0e9b", null ],
    [ "MatrixUpdate", "group__led__mat.html#ga364f2f29407f1a2929ac28ef7f66967c", null ],
    [ "ResetTick", "group__led__mat.html#gaa49af1a0dcfe233c5f64f338c0b87045", null ],
    [ "SetTick", "group__led__mat.html#ga0d10998abd193081ef08c6eddff125d2", null ],
    [ "TransformPoint", "group__led__mat.html#ga3b470268d354d09cf3693781949a5dc0", null ],
    [ "move_delta", "group__led__mat.html#ga9310d6dd1e86f0d38b7a5e780111ed6f", null ],
    [ "tick", "group__led__mat.html#ga9bd8f483b6df66ebb59f5d8cbbec7b40", null ]
];