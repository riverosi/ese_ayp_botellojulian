var structnrf24l01__t =
[
    [ "ce", "structnrf24l01__t.html#ae97decb2024abfa2ae316df1755cd693", null ],
    [ "cs", "structnrf24l01__t.html#a052dbd9d1287ace89cad19239d8ec967", null ],
    [ "en_ack_pay", "structnrf24l01__t.html#aee16a7c90daf97a46e9634baa152b233", null ],
    [ "irq", "structnrf24l01__t.html#a557e9320e354f2ca799807f648fea89b", null ],
    [ "mode", "structnrf24l01__t.html#a12fc9ee2c49790f449e9e008ded54fb4", null ],
    [ "pin_int_num", "structnrf24l01__t.html#a28f4e2f455cd6b91bb7d6d20e6a41d93", null ],
    [ "spi", "structnrf24l01__t.html#a8ff850ae784fee55f172a282f9624caf", null ]
];