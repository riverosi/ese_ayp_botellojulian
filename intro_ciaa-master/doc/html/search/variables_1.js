var searchData=
[
  ['ce',['ce',['../structnrf24l01__t.html#ae97decb2024abfa2ae316df1755cd693',1,'nrf24l01_t']]],
  ['cfg',['cfg',['../structspi_device__t.html#af43808b31cb9304148f732f2ecb8fafd',1,'spiDevice_t']]],
  ['clock_5ffreq',['clock_freq',['../structspi_config__t.html#a456fb9ec3c621b0f79a86652202b3b25',1,'spiConfig_t']]],
  ['clock_5fmode',['clock_mode',['../structspi_config__t.html#ab93d2bd987b4d6d6623f0afd9b19e48c',1,'spiConfig_t::clock_mode()'],['../group__spi__master__hal.html#ga992f05ee30e4e4169f4af9adf658ff57',1,'clock_mode():&#160;spi_master_hal.c']]],
  ['cs',['cs',['../structmax7219__t.html#a052dbd9d1287ace89cad19239d8ec967',1,'max7219_t::cs()'],['../structnrf24l01__t.html#a052dbd9d1287ace89cad19239d8ec967',1,'nrf24l01_t::cs()']]]
];
