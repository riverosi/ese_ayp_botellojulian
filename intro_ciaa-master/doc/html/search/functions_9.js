var searchData=
[
  ['settick',['SetTick',['../group__led__mat.html#ga0d10998abd193081ef08c6eddff125d2',1,'led_matrix.c']]],
  ['spiconfig',['SpiConfig',['../group__spi__master__hal.html#gaf259cdbd1a09099217c9884abed51b11',1,'spi_master_hal.c']]],
  ['spideinit',['SpiDeInit',['../group__spi__master__hal.html#ga7c1ba864e3e51caa3c9ca168359751f0',1,'spi_master_hal.c']]],
  ['spidevclearirqlist',['SpiDevClearIrqList',['../group__spi__generic__device.html#ga64f4d33a93ea37d8246152279585fc8a',1,'spi_generic_device.c']]],
  ['spidevdeinit',['SpiDevDeInit',['../group__spi__generic__device.html#ga4959a71b38f0c8006956e56068c11a1c',1,'spi_generic_device.c']]],
  ['spidevinit',['SpiDevInit',['../group__spi__generic__device.html#gae142ee498a0f89e26c80cfb28d9848dd',1,'spi_generic_device.c']]],
  ['spidevmake2bpacket',['SpiDevMake2BPacket',['../group__spi__generic__device.html#gaec8e620ea642cb4bb5b0d0fd2bae3cbc',1,'spi_generic_device.c']]],
  ['spidevreadblocking',['SpiDevReadBlocking',['../group__spi__generic__device.html#ga8c06151eed20f10d8421fe9c07f0c9d1',1,'spi_generic_device.c']]],
  ['spidevrwblocking',['SpiDevRWBlocking',['../group__spi__generic__device.html#ga1e261acd94e656abb6f87c7028aa5367',1,'spi_generic_device.c']]],
  ['spidevsetirqlist',['SpiDevSetIrqList',['../group__spi__generic__device.html#ga6547e9e96b8797e631282e5c6a20a9fe',1,'spi_generic_device.c']]],
  ['spidevwriteblocking',['SpiDevWriteBlocking',['../group__spi__generic__device.html#gac0d88ca0371e3131d7be1160ab1510f6',1,'spi_generic_device.c']]],
  ['spiinit',['SpiInit',['../group__spi__master__hal.html#gaf5908b2588fbfad3a6c0cf6690c7211c',1,'spi_master_hal.c']]],
  ['spireadblocking',['SpiReadBlocking',['../group__spi__master__hal.html#gad83fa898adc7de23bb8dc5223d7d6a89',1,'spi_master_hal.c']]],
  ['spirwblocking',['SpiRWBlocking',['../group__spi__master__hal.html#gaa15b15b3168e1e3e8592b089df9edb77',1,'spi_master_hal.c']]],
  ['spitestloopback',['SpiTestLoopBack',['../group__spi__master__hal.html#ga9e9dc6d104c30c6d1ab6e6dbc744b80b',1,'spi_master_hal.c']]],
  ['spiwrite',['SpiWrite',['../group__max7219.html#ga34cac747bdd27445c7760b4d19f15c0f',1,'max7219.c']]],
  ['spiwriteblocking',['SpiWriteBlocking',['../group__spi__master__hal.html#ga5a97628b47a87690f7654243e3a98f6b',1,'spi_master_hal.c']]],
  ['systeminit',['SystemInit',['../system__init_8h.html#a93f514700ccf00d08dbdcff7f1224eb2',1,'SystemInit(void):&#160;system_init.c'],['../system__init_8c.html#a93f514700ccf00d08dbdcff7f1224eb2',1,'SystemInit(void):&#160;system_init.c']]],
  ['systick_5fhandler',['SysTick_Handler',['../_i_s_r_8h.html#ab5e09814056d617c521549e542639b7e',1,'SysTick_Handler(void):&#160;ISR.c'],['../_i_s_r_8c.html#ab5e09814056d617c521549e542639b7e',1,'SysTick_Handler(void):&#160;ISR.c']]],
  ['systickinit',['SysTickInit',['../system__init_8h.html#a2dfb15bc3d9e9257f1c7a118141c81d8',1,'SysTickInit(uint32_t ticks_per_sec):&#160;system_init.c'],['../system__init_8c.html#a2dfb15bc3d9e9257f1c7a118141c81d8',1,'SysTickInit(uint32_t ticks_per_sec):&#160;system_init.c']]]
];
