var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstuvxy",
  1: "dglmns",
  2: "bdgilmnsu",
  3: "cdegilmnrstuv",
  4: "bcdefghilmnoprstxy",
  5: "hm",
  6: "glmns",
  7: "giprstxy",
  8: "ailnos",
  9: "cglmnpsu",
  10: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

