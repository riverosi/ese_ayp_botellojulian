var searchData=
[
  ['secondary_5fdev',['secondary_dev',['../group___n_r_f24_l01.html#ga6bcf935edeb054fc4fe04339f1b10b81',1,'nrf24l01.c']]],
  ['snd_5fto_5fprx',['snd_to_PRX',['../group___n_r_f24_l01.html#ga4853746f1882bc0dd34b94f41b5872d0',1,'snd_to_PRX():&#160;nrf24l01.c'],['../group___n_r_f24_l01.html#ga4d2d6e90fa46717ae9e86ee5079bcf36',1,'snd_to_PRX():&#160;nrf24l01.c']]],
  ['snd_5fto_5fptx',['snd_to_PTX',['../group___n_r_f24_l01.html#ga30b1b406429199309de445b14ddb3cfd',1,'snd_to_PTX():&#160;nrf24l01.c'],['../group___n_r_f24_l01.html#ga4c5193fd651c8de27e98c2523fe78cca',1,'snd_to_PTX():&#160;nrf24l01.c']]],
  ['spi',['spi',['../structmax7219__t.html#a8ff850ae784fee55f172a282f9624caf',1,'max7219_t::spi()'],['../structnrf24l01__t.html#a8ff850ae784fee55f172a282f9624caf',1,'nrf24l01_t::spi()']]],
  ['ssp',['ssp',['../structspi_config__t.html#a87f2f5225e4a6e263cf789b962a2a4b4',1,'spiConfig_t::ssp()'],['../group__spi__master__hal.html#gae153c50568c2937b883f2fd9de1c385a',1,'ssp():&#160;spi_master_hal.c']]],
  ['ssp_5f1_5fcfg',['ssp_1_cfg',['../group__spi__master__hal.html#gabc493371d223b2b6b410e281a7b60086',1,'spi_master_hal.c']]],
  ['ssp_5fcfg',['ssp_cfg',['../group__spi__master__hal.html#gabbef5cf3f30628b5f9d6bbd64b7f82a4',1,'spi_master_hal.c']]],
  ['st_5fpt',['st_pt',['../structled_matrix__t.html#a53d5dd8e731539b8d08805811ab503df',1,'ledMatrix_t']]],
  ['staged',['staged',['../structstaged_point__t.html#ac88754e6c216c0fc81f65fea928e15b7',1,'stagedPoint_t']]],
  ['status',['status',['../structstaged_point__t.html#ab73bed1b4a9109d214c0752bbdd149f5',1,'stagedPoint_t']]]
];
