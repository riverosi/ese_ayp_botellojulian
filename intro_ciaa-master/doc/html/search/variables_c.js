var searchData=
[
  ['pin',['pin',['../structgpio_cfg__t.html#ab40a673fb19c1e650e1f79de91788aa5',1,'gpioCfg_t::pin()'],['../group__spi__master__hal.html#gab40a673fb19c1e650e1f79de91788aa5',1,'sspCfg_t::pin()']]],
  ['pin_5fint',['pin_int',['../group__gpio__hal.html#gabba10597874c82dff0deef7035bab65d',1,'gpio_hal.c']]],
  ['pin_5fint_5fnum',['pin_int_num',['../structnrf24l01__t.html#a28f4e2f455cd6b91bb7d6d20e6a41d93',1,'nrf24l01_t']]],
  ['port',['port',['../structgpio_cfg__t.html#a2fa54f9024782843172506fadbee2ac8',1,'gpioCfg_t::port()'],['../group__spi__master__hal.html#ga2fa54f9024782843172506fadbee2ac8',1,'sspCfg_t::port()']]],
  ['primary_5fdev',['primary_dev',['../group___n_r_f24_l01.html#ga595902709be8179b975cfd5c35a11cf8',1,'nrf24l01.c']]]
];
