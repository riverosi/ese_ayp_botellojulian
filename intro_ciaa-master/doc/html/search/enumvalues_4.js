var searchData=
[
  ['set_5fpoint',['SET_POINT',['../group__max7219.html#gga2fa196c9c3c1c7fadaa9c5687b7c4b34ac377fe3e636fce24d1a88b67385a9634',1,'max7219.h']]],
  ['spi_5fbits16',['SPI_BITS16',['../group__spi__master__hal.html#ggab9b451569dd68389a74ba69dd117b90da5c0bfb13271d3c8cfdb4cd37d4fe651f',1,'spi_master_hal.h']]],
  ['spi_5fbits8',['SPI_BITS8',['../group__spi__master__hal.html#ggab9b451569dd68389a74ba69dd117b90da10bd39ac136b1c1b579663ac1d06c382',1,'spi_master_hal.h']]],
  ['spi_5fclk_5fmode0',['SPI_CLK_MODE0',['../group__spi__master__hal.html#ggaedec30f3ece57338dca835603aa676d3a067b0322b09ac4d3cc14bbf2995a8100',1,'spi_master_hal.h']]],
  ['spi_5fclk_5fmode1',['SPI_CLK_MODE1',['../group__spi__master__hal.html#ggaedec30f3ece57338dca835603aa676d3a3680a418c288817b3eb426e5b5e8ae31',1,'spi_master_hal.h']]],
  ['spi_5fclk_5fmode2',['SPI_CLK_MODE2',['../group__spi__master__hal.html#ggaedec30f3ece57338dca835603aa676d3ac0d2a9655586fb948b9fd9241810a691',1,'spi_master_hal.h']]],
  ['spi_5fclk_5fmode3',['SPI_CLK_MODE3',['../group__spi__master__hal.html#ggaedec30f3ece57338dca835603aa676d3a1df2995a001f248aada529c305ae14b7',1,'spi_master_hal.h']]],
  ['ssp_5f0',['SSP_0',['../group__spi__master__hal.html#gga279b37610b2c3c0606c6f7a19c61d0d6a82824bff0ac36361f859d0dcea8b86be',1,'spi_master_hal.h']]],
  ['ssp_5f1',['SSP_1',['../group__spi__master__hal.html#gga279b37610b2c3c0606c6f7a19c61d0d6a90e73f823d2f1ec469a4662d743ec1f3',1,'spi_master_hal.h']]]
];
