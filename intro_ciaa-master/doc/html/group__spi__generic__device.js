var group__spi__generic__device =
[
    [ "spiDevice_t", "structspi_device__t.html", [
      [ "cfg", "structspi_device__t.html#af43808b31cb9304148f732f2ecb8fafd", null ],
      [ "id", "structspi_device__t.html#a1e6927fa1486224044e568f9c370519b", null ]
    ] ],
    [ "MAX_PIN_INT", "group__spi__generic__device.html#ga7766bea149ab8580dbb2ba2efd4e5589", null ],
    [ "NONE", "group__spi__generic__device.html#ga655c84af1b0034986ff56e12e84f983d", null ],
    [ "DisableIrqs", "group__spi__generic__device.html#gaf5b5fb89630e03012cd2f855e21b932b", null ],
    [ "EnableIrqs", "group__spi__generic__device.html#ga994dc3273127c41e1ebb51923789f295", null ],
    [ "SpiDevClearIrqList", "group__spi__generic__device.html#ga64f4d33a93ea37d8246152279585fc8a", null ],
    [ "SpiDevDeInit", "group__spi__generic__device.html#ga4959a71b38f0c8006956e56068c11a1c", null ],
    [ "SpiDevInit", "group__spi__generic__device.html#gae142ee498a0f89e26c80cfb28d9848dd", null ],
    [ "SpiDevMake2BPacket", "group__spi__generic__device.html#gaec8e620ea642cb4bb5b0d0fd2bae3cbc", null ],
    [ "SpiDevReadBlocking", "group__spi__generic__device.html#ga8c06151eed20f10d8421fe9c07f0c9d1", null ],
    [ "SpiDevRWBlocking", "group__spi__generic__device.html#ga1e261acd94e656abb6f87c7028aa5367", null ],
    [ "SpiDevSetIrqList", "group__spi__generic__device.html#ga6547e9e96b8797e631282e5c6a20a9fe", null ],
    [ "SpiDevWriteBlocking", "group__spi__generic__device.html#gac0d88ca0371e3131d7be1160ab1510f6", null ],
    [ "enabled_irqs", "group__spi__generic__device.html#ga573c4ac89ced0faca3edf0779c324616", null ],
    [ "id_generator", "group__spi__generic__device.html#ga052e9bc490a81b3d503e4163593eb42e", null ],
    [ "interrupts", "group__spi__generic__device.html#gad5077908d443f9906c51cf551d81a508", null ],
    [ "last_dev_used", "group__spi__generic__device.html#ga5f09e223751b1b7e94bf922b6090a90b", null ]
];