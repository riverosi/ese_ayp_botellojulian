var group___baremetal =
[
    [ "lpc4337", "group___baremetal.html#gadfc13aced9eecd5bf67ab539639ef200", null ],
    [ "mk60fx512vlq15", "group___baremetal.html#gac5996bc3bcae001e239c5563704c0d7d", null ],
    [ "SWITCHES", "group___baremetal.html#gaa87203a5637fb4759a378b579aaebff6", [
      [ "TECLA1", "group___baremetal.html#ggaa87203a5637fb4759a378b579aaebff6a7d2d4d7767c90c023bcf91d65858950b", null ],
      [ "TECLA2", "group___baremetal.html#ggaa87203a5637fb4759a378b579aaebff6a022212c2b30d036df890b08f2f196e05", null ],
      [ "TECLA3", "group___baremetal.html#ggaa87203a5637fb4759a378b579aaebff6abf856f5a128061ca7a000f91c022231e", null ],
      [ "TECLA4", "group___baremetal.html#ggaa87203a5637fb4759a378b579aaebff6a216161338f97a094bec10dbabe5dcbe4", null ]
    ] ],
    [ "Init_Switches", "group___baremetal.html#ga567c9e752c8f560960a5bacce2f3cc04", null ],
    [ "Read_Switches", "group___baremetal.html#ga062112e932fd77a1fa64764587b1df98", null ]
];