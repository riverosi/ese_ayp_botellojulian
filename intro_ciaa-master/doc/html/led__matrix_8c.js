var led__matrix_8c =
[
    [ "MatrixBlank", "group__led__mat.html#gacc7f8d027e38813980af8bf134b9e24d", null ],
    [ "MatrixGetImage", "group__led__mat.html#gaa5618e59acffe7f04d4168e560c75835", null ],
    [ "MatrixGetPoint", "group__led__mat.html#gab2e9461cb0e3509c155ec3192ea5232d", null ],
    [ "MatrixInit", "group__led__mat.html#ga710c4a81ac95f201bcb246ec77bf75e7", null ],
    [ "MatrixMoveStagedPoint", "group__led__mat.html#gaebd46eae4b0cca4406e913d69e8ca9a9", null ],
    [ "MatrixOnTickUpdateSP", "group__led__mat.html#ga9c951795b21797dcb03bb0344e44ea4c", null ],
    [ "MatrixRotate", "group__led__mat.html#gae8350cc8eac7b6dd3c8ca49bb7a34c35", null ],
    [ "MatrixRstPoint", "group__led__mat.html#gae8cf6273cf81467a7f178274c17274ad", null ],
    [ "MatrixSetImage", "group__led__mat.html#gadafcc6eec23e941ba9549d83bcbac6a4", null ],
    [ "MatrixSetPoint", "group__led__mat.html#gae5a78e5cb7bd39f23c6d37f0bb4c4283", null ],
    [ "MatrixStagePoint", "group__led__mat.html#ga7da8758105d08e9fdeb35ee2dfedc418", null ],
    [ "MatrixTick", "group__led__mat.html#ga9b101c2492f0472e9bc8d6897eafae00", null ],
    [ "MatrixTogPoint", "group__led__mat.html#gaa9ae3dad7a9afa20577e75b77e6bec1a", null ],
    [ "MatrixTogStagePoint", "group__led__mat.html#ga6e8b5f951e0d82ebc6127f40fa18a1fc", null ],
    [ "MatrixUnstagePoint", "group__led__mat.html#ga640f9271eda47539d7ff7742715c0e9b", null ],
    [ "MatrixUpdate", "group__led__mat.html#ga364f2f29407f1a2929ac28ef7f66967c", null ],
    [ "ResetTick", "group__led__mat.html#gaa49af1a0dcfe233c5f64f338c0b87045", null ],
    [ "SetTick", "group__led__mat.html#ga0d10998abd193081ef08c6eddff125d2", null ],
    [ "TransformPoint", "group__led__mat.html#ga3b470268d354d09cf3693781949a5dc0", null ],
    [ "move_delta", "group__led__mat.html#ga9310d6dd1e86f0d38b7a5e780111ed6f", null ],
    [ "tick", "group__led__mat.html#ga9bd8f483b6df66ebb59f5d8cbbec7b40", null ]
];