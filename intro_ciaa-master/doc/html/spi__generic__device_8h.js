var spi__generic__device_8h =
[
    [ "SpiDevClearIrqList", "group__spi__generic__device.html#ga64f4d33a93ea37d8246152279585fc8a", null ],
    [ "SpiDevDeInit", "group__spi__generic__device.html#ga4959a71b38f0c8006956e56068c11a1c", null ],
    [ "SpiDevInit", "group__spi__generic__device.html#gae142ee498a0f89e26c80cfb28d9848dd", null ],
    [ "SpiDevMake2BPacket", "group__spi__generic__device.html#gaec8e620ea642cb4bb5b0d0fd2bae3cbc", null ],
    [ "SpiDevReadBlocking", "group__spi__generic__device.html#ga8c06151eed20f10d8421fe9c07f0c9d1", null ],
    [ "SpiDevRWBlocking", "group__spi__generic__device.html#ga1e261acd94e656abb6f87c7028aa5367", null ],
    [ "SpiDevSetIrqList", "group__spi__generic__device.html#ga6547e9e96b8797e631282e5c6a20a9fe", null ],
    [ "SpiDevWriteBlocking", "group__spi__generic__device.html#gac0d88ca0371e3131d7be1160ab1510f6", null ]
];