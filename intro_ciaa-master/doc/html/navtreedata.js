var NAVTREE =
[
  [ "Wireless Turn System", "index.html", [
    [ "Main Page", "index.html", [
      [ "Application: Wireless Turn System", "index.html#app", null ],
      [ "Demonstration video", "index.html#demo", null ],
      [ "Interface description", "index.html#iface", null ],
      [ "Development Set Up", "index.html#setup", null ],
      [ "Hardware used", "index.html#hw", null ],
      [ "Wiring", "index.html#wiring", null ],
      [ "Application and lower layers (drivers and utils)", "index.html#layers", null ]
    ] ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_i_s_r_8c.html",
"group___n_r_f24_l01.html#ga6ca6b0a60c7ce66a12fd6e2ac6f785d3",
"group___n_r_f24_l01.html#gafb41073531e46750c84f4669d3e24d1a",
"group__leds.html#gga1f3289eeddfbcff1515a3786dc0518faa9924710c1e80e5287b87b1d6541c3623",
"led_8c.html#a13af42ce5888d48b000d3f4cfd538ede"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';