var group__switches =
[
    [ "lpc4337", "group__switches.html#gadfc13aced9eecd5bf67ab539639ef200", null ],
    [ "mk60fx512vlq15", "group__switches.html#gac5996bc3bcae001e239c5563704c0d7d", null ],
    [ "SWITCHES", "group__switches.html#gaa87203a5637fb4759a378b579aaebff6", [
      [ "TECLA1", "group__switches.html#ggaa87203a5637fb4759a378b579aaebff6a7d2d4d7767c90c023bcf91d65858950b", null ],
      [ "TECLA2", "group__switches.html#ggaa87203a5637fb4759a378b579aaebff6a022212c2b30d036df890b08f2f196e05", null ],
      [ "TECLA3", "group__switches.html#ggaa87203a5637fb4759a378b579aaebff6abf856f5a128061ca7a000f91c022231e", null ],
      [ "TECLA4", "group__switches.html#ggaa87203a5637fb4759a378b579aaebff6a216161338f97a094bec10dbabe5dcbe4", null ]
    ] ],
    [ "Init_Switches", "group__switches.html#ga567c9e752c8f560960a5bacce2f3cc04", null ],
    [ "Read_Switches", "group__switches.html#ga062112e932fd77a1fa64764587b1df98", null ]
];