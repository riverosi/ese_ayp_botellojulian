var led__matrix_8h =
[
    [ "MATRIX_SIZE", "group__led__mat.html#ga2bd32349fcbeb6ee86434a65226cba1a", null ],
    [ "matrixOrientation_t", "group__led__mat.html#ga8fb070c465918a95ee2d3b296fe65e74", [
      [ "ROT_0_CW", "group__led__mat.html#gga8fb070c465918a95ee2d3b296fe65e74a9d77c4964b97af8590d8c7cd86be1a5f", null ],
      [ "ROT_90_CW", "group__led__mat.html#gga8fb070c465918a95ee2d3b296fe65e74ab0ff4aa7e9875eb64a828407d29b5716", null ],
      [ "ROT_180_CW", "group__led__mat.html#gga8fb070c465918a95ee2d3b296fe65e74ac50c1e5c993750be5c8d518a48e9e1cf", null ],
      [ "ROT_270_CW", "group__led__mat.html#gga8fb070c465918a95ee2d3b296fe65e74a1326c3ce26fb0b9835998d315cb5592c", null ]
    ] ],
    [ "moveDir_t", "group__led__mat.html#ga42e118908eddedc60d4b6f9e713f9a12", [
      [ "X_PLUS_1", "group__led__mat.html#gga42e118908eddedc60d4b6f9e713f9a12a8b658a435001622ba70a14c1d91f3186", null ],
      [ "X_MINUS_1", "group__led__mat.html#gga42e118908eddedc60d4b6f9e713f9a12a527f59e15a0515389d4b45423455a165", null ],
      [ "Y_PLUS_1", "group__led__mat.html#gga42e118908eddedc60d4b6f9e713f9a12a1a012ae8dd554c6c2a3378e0698ee554", null ],
      [ "Y_MINUS_1", "group__led__mat.html#gga42e118908eddedc60d4b6f9e713f9a12a9f4f5e49b36f98919ef2ac407b7ef2b8", null ]
    ] ],
    [ "MatrixBlank", "group__led__mat.html#gacc7f8d027e38813980af8bf134b9e24d", null ],
    [ "MatrixGetImage", "group__led__mat.html#gaa5618e59acffe7f04d4168e560c75835", null ],
    [ "MatrixGetPoint", "group__led__mat.html#gab2e9461cb0e3509c155ec3192ea5232d", null ],
    [ "MatrixInit", "group__led__mat.html#ga710c4a81ac95f201bcb246ec77bf75e7", null ],
    [ "MatrixMoveStagedPoint", "group__led__mat.html#gaebd46eae4b0cca4406e913d69e8ca9a9", null ],
    [ "MatrixOnTickUpdateSP", "group__led__mat.html#ga9c951795b21797dcb03bb0344e44ea4c", null ],
    [ "MatrixRotate", "group__led__mat.html#gae8350cc8eac7b6dd3c8ca49bb7a34c35", null ],
    [ "MatrixRstPoint", "group__led__mat.html#gae8cf6273cf81467a7f178274c17274ad", null ],
    [ "MatrixSetImage", "group__led__mat.html#gadafcc6eec23e941ba9549d83bcbac6a4", null ],
    [ "MatrixSetPoint", "group__led__mat.html#gae5a78e5cb7bd39f23c6d37f0bb4c4283", null ],
    [ "MatrixStagePoint", "group__led__mat.html#ga7da8758105d08e9fdeb35ee2dfedc418", null ],
    [ "MatrixTick", "group__led__mat.html#ga9b101c2492f0472e9bc8d6897eafae00", null ],
    [ "MatrixTogPoint", "group__led__mat.html#gaa9ae3dad7a9afa20577e75b77e6bec1a", null ],
    [ "MatrixTogStagePoint", "group__led__mat.html#ga6e8b5f951e0d82ebc6127f40fa18a1fc", null ],
    [ "MatrixUnstagePoint", "group__led__mat.html#ga640f9271eda47539d7ff7742715c0e9b", null ],
    [ "MatrixUpdate", "group__led__mat.html#ga364f2f29407f1a2929ac28ef7f66967c", null ]
];