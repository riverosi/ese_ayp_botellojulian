#ifndef PROJECTS_RF_TURN_COUNTER_INC_ISR_H_
#define PROJECTS_RF_TURN_COUNTER_INC_ISR_H_

/* 1 to activate */
#define asRX 0
#define asTX 0

void SysTick_Handler(void);

#if asTX
void GPIO0_IRQHandler(void);
#endif

#if asRX
void GPIO1_IRQHandler(void);
#endif

#endif /* PROJECTS_RF_TURN_COUNTER_INC_ISR_H_ */
