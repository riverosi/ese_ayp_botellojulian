/* Copyright 2018, Eduardo Filomena - Gonzalo Cuenca
 * Copyright 2018, Julián Botello <jlnbotello@gmail.com>
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \mainpage Main Page
 *
 * @date 27/12/2018
 * @author Julián Botello <jlnbotello@gmail.com>
 * @section app Application: Wireless Turn System
 *
 * The 'wireless turn system' is composed of a remote control and a display (2 led matrix).
 * The remote control allows you to change de number showed on the display. It's possible
 * to decrement, increment and reset the counter. The counter has a range from 0 to 99.
 *
 * ---------------------
 *
 * @section demo Demonstration video
 *
 * \htmlonly
 * <div align="center">
 * <iframe width="720" height="405"
 * src="https://www.youtube.com/embed/EFcKnwYQoi8"
 * frameborder="0" allow="accelerometer; autoplay;
 * encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
 * </iframe>
 * </div>
 * \endhtmlonly
 *
 * @link https://youtu.be/EFcKnwYQoi8 @endlink
 *
 * ---------------------
 * @section iface Interface description
 *
 * ##Remote control side - PTX##
 * 	###KEYS###
 * + KEY1 	Decrements the counter
 * + KEY2	Increments the counter
 * + KEY3 	Resets the counter to zero
 * + KEY4 	Easter Egg: 'FIUNER' message
 *
 *	###LEDS###
 *	Turns on the led associated with the button pressed when acknowledge is received
 *
 * ##Counter display side - PRX##
 * 	###KEYS###
 * No action
 *
 * ###LEDS###
 * Turns on the led associated with the button pressed on the remote control
 *
 * ---------------------
 *
 * @section setup Development Set Up
 * \image html WTS.jpeg
 *
 * ---------------------
 *
 * @section hw Hardware used
 *  + 2 EDU-CIAA development board
 *  + 2 led matrix (MAX7219)
 *  + 2 transceivers (NRF24L01)
 *
 * ---------------------
 *
 * @section wiring Wiring
 * ##Primary Transmitter NRF24L01- PTX##
 *
 * | NRF24L01 pins (PTX) | EDU-CIAA pins |
 * |:-------------------:|:-------------:|
 * |         VCC         |    +3.3V      |
 * |         GND         |    GND        |
 * |         CSN         |    GPIO5      |
 * |         CE	        |    GPIO7      |
 * |         SCK         |    SPI_SCK    |
 * |         MOSI        |    SPI_MOSI   |
 * |         IRQ         |    GPIO8      |
 * |         MISO        |    SPI_MISO   |
 *
 * ##Primary Receiver NRF24L01- PRX##
 *
 * | NRF24L01 pins (PRX) | EDU-CIAA pins |
 * |:-------------------:|:-------------:|
 * |         VCC         |    +3.3V      |
 * |         GND         |    GND        |
 * |         CSN         |    GPIO2      |
 * |         CE          |    GPIO4      |
 * |         SCK         |    SPI_SCK    |
 * |         MOSI        |    SPI_MOSI   |
 * |         IRQ         |    GPIO6      |
 * |         MISO        |    SPI_MISO   |
 *
 * ## Most Significant Digit Led Matrix MAX7219 ##
 *
 * | Led matrix MAX7219 pins | EDU-CIAA pins |
 * |:-----------------------:|:-------------:|
 * |         VCC        		|    +5.0V      |
 * |         GND        		|    GND        |
 * |         CS         		|    GPIO0      |
 * |         CLK        		|    SPI_SCK    |
 * |         DIN        		|    SPI_MOSI   |
 *
 * ## Least Significant Digit Led Matrix MAX7219 ##
 *
 * | Led matrix MAX7219 pins | EDU-CIAA pins |
 * |:-----------------------:|:-------------:|
 * |         VCC        		|    +5.0V      |
 * |         GND        		|    GND        |
 * |         CS         		|    GPIO1      |
 * |         CLK        		|    SPI_SCK    |
 * |         DIN        		|    SPI_MOSI   |
 *
 * ---------------------
 *
 * @section layers Application and lower layers (drivers and utils)
 *
 * \image html layers.jpeg
 *
 */

/*==================[inclusions]=============================================*/

#include "system_init.h"
#include "nrf24l01.h"
#include "led_matrix.h"
#include "led.h"
#include "switch.h"
#include "stopwatch.h"
#include "ISR.h"

/*==================[macros and definitions]=================================*/

/* 1 to activate */
#define asRX 0 // IMPORTANT -> modify this flags on ISR.h too
#define asTX 0 // IMPORTANT -> modify this flags on ISR.h too

/*==================[internal data definition]===============================*/

#if asRX

static const uint64_t number[] = {
	0x3c66666e76663c00,	/* 0 */
	0x7e1818181c181800,	/* 1 */
	0x7e060c3060663c00, /* 2 */
	0x3c66603860663c00, /* 3 */
	0x30307e3234383000, /* 4 */
	0x3c6660603e067e00, /* 5 */
	0x3c66663e06663c00, /* 6 */
	0x1818183030667e00, /* 7 */
	0x3c66663c66663c00, /* 8 */
	0x3c66607c66663c00  /* 9 */
};

static const int NUMBER_LEN = sizeof(number)/sizeof(uint64_t);

static const uint64_t FIUNER[] = {
	0x000f06161e16467f, /* F */
	0x001e0c0c0c0c0c1e, /* I */
	0x003f333333333333, /* U */
	0x006363737b6f6763, /* N */
	0x007f46161e16467f, /* E */
	0x006766363e66663f, /* R */
};
#endif

/*==================[internal functions declaration]==========================*/

#if asRX
void ShowNumber(ledMatrix_t *msd_mat, ledMatrix_t *lsd_mat, uint8_t number);
#endif

/*==================[   		MAIN			]==========================*/

int main(void) {

	SystemInit();
	StopWatch_Init();
	Init_Leds();
	Init_Switches();

#if asTX
	nrf24l01_t TX;
	TX.spi.cfg=nrf24l01_spi_default_cfg;
	TX.cs.n=GPIO_5;
	TX.ce.n=GPIO_7;
	TX.irq.n=GPIO_8;
	TX.mode=PTX;
	TX.en_ack_pay=TRUE;
	TX.pin_int_num=0; /*Handler will be GPIO0_IRQHandler when TX.irq.n interrupt*/

	Nrf24Init(&TX);

	/* Enable ack payload */
	Nrf24EnableFeatureAckPL(&TX);

	Nrf24PrimaryDevISRConfig(&TX);

	uint8_t key=0;
#endif

#if asRX
	nrf24l01_t RX;
	RX.spi.cfg=nrf24l01_spi_default_cfg;
	RX.cs.n=GPIO_2;
	RX.ce.n=GPIO_4;
	RX.irq.n=GPIO_6;
	RX.mode=PRX;
	RX.en_ack_pay=TRUE;
	RX.pin_int_num=1; /*Handler will be GPIO1_IRQHandler when TX.irq.n interrupt*/
	SpiSetIrqList(1); /* This IRQ will be disable when SPI is been used */

	Nrf24Init(&RX);

	/* Enable ack payload */
	Nrf24EnableFeatureAckPL(&RX);

	/* Set the first ack payload */
	uint8_t first_ack[21] = "First ack received!!!";
	Nrf24SetAckPayload(&RX,first_ack, 0x00, 21);

	/* Enable RX mode */
	Nrf24EnableRxMode(&RX);

	Nrf24SecondaryDevISRConfig(&RX); // GPIO5_IRQHandler



	max7219_t msd_dev;
	Max7219Init(&msd_dev, GPIO_0, max7219_spi_default_cfg);
	max7219_t lsd_dev;
	Max7219Init(&lsd_dev, GPIO_1, max7219_spi_default_cfg);

	ledMatrix_t ms_digit;
	MatrixInit(&ms_digit, msd_dev, ROT_90_CW);
	ledMatrix_t ls_digit;
	MatrixInit(&ls_digit, lsd_dev, ROT_90_CW);
#endif

	/* Activates SysTick: TX send data regulary.
	 * Although this is not the frequency of sending. See systick handler
	 */
	SysTickInit(1000); /* 1000 ticks per second -> 1ms */

#if asRX
	uint8_t counter=0;
#endif
	while(1){

#if asTX
		key=Read_Switches();

		snd_to_PRX[0]=key;

		/* Turns on led associated with button if acknowledge is received */
		if(rcv_fr_PRX[0]==1){
			Led_On(RGB_B_LED);
			StopWatch_DelayMs(1000);
			Led_Off(RGB_B_LED);
			rcv_fr_PRX[0]=0;
		}
		if(rcv_fr_PRX[0]==2){
			Led_On(RED_LED);
			StopWatch_DelayMs(1000);
			Led_Off(RED_LED);
			rcv_fr_PRX[0]=0;
		}
		if(rcv_fr_PRX[0]==4){
			Led_On(YELLOW_LED);
			StopWatch_DelayMs(1000);
			Led_Off(YELLOW_LED);
			rcv_fr_PRX[0]=0;
		}
		if(rcv_fr_PRX[0]==8){
			Led_On(GREEN_LED);
			StopWatch_DelayMs(1000);
			Led_Off(GREEN_LED);
			rcv_fr_PRX[0]=0;
		}
		StopWatch_DelayMs(500);
#endif

#if asRX
		/* Turns on led associated with button if data is received from PTX */
		if(rcv_fr_PTX[0]==1){

			if(counter>0){
				counter--;
			}
			ShowNumber(&ms_digit,&ls_digit,counter);
			snd_to_PTX[0]=1;
			Led_On(RGB_B_LED);
			StopWatch_DelayMs(100);
			Led_Off(RGB_B_LED);
		}

		if(rcv_fr_PTX[0]==2){

			if(counter<99){
				counter++;
			}
			ShowNumber(&ms_digit,&ls_digit,counter);
			snd_to_PTX[0]=2;
			Led_On(RED_LED);
			StopWatch_DelayMs(100);
			Led_Off(RED_LED);
		}

		if(rcv_fr_PTX[0]==4){
			counter=0;
			ShowNumber(&ms_digit,&ls_digit,counter);
			snd_to_PTX[0]=4;
			Led_On(YELLOW_LED);
			StopWatch_DelayMs(100);
			Led_Off(YELLOW_LED);

		}

		if(rcv_fr_PTX[0]==8){

			MatrixSetImage(&ms_digit,FIUNER[0]);
			MatrixSetImage(&ls_digit,FIUNER[1]);
			MatrixUpdate(&ms_digit);
			MatrixUpdate(&ls_digit);
			StopWatch_DelayMs(1000);
			MatrixSetImage(&ms_digit,FIUNER[2]);
			MatrixSetImage(&ls_digit,FIUNER[3]);
			MatrixUpdate(&ms_digit);
			MatrixUpdate(&ls_digit);
			StopWatch_DelayMs(1000);
			MatrixSetImage(&ms_digit,FIUNER[4]);
			MatrixSetImage(&ls_digit,FIUNER[5]);
			MatrixUpdate(&ms_digit);
			MatrixUpdate(&ls_digit);
			StopWatch_DelayMs(1000);
			ShowNumber(&ms_digit,&ls_digit,counter);
			snd_to_PTX[0]=8;
			Led_On(GREEN_LED);
			StopWatch_DelayMs(100);
			Led_Off(GREEN_LED);
		}
		StopWatch_DelayMs(1000);
#endif

	}

	return 0;


}

/*==================[internal functions definition]==========================*/

#if asRX



void ShowNumber(ledMatrix_t *msd_mat, ledMatrix_t *lsd_mat, uint8_t num) {
	num = num % 100; /* Range: 0 to 99 */

	uint8_t ls_digit = num % 10;
	uint8_t ms_digit = num / 10;

	MatrixSetImage(msd_mat, number[ms_digit]);
	MatrixSetImage(lsd_mat, number[ls_digit]);

	MatrixUpdate(msd_mat);
	MatrixUpdate(lsd_mat);
}

#endif



/*==================[end of file]============================================*/

