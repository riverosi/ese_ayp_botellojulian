/* Copyright (c) 2018 Julián Botello <jlnbotello@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @addtogroup debug_uart UART debugging
 *  @brief Debugging utilities for USART2 on EDU-CIAA NXP
 *  @{
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_BM_INC_UART_DEBUG_H_
#define MODULES_LPC4337_M4_DRIVERS_BM_INC_UART_DEBUG_H_

/*==================[inclusions]=============================================*/

#include "drivers_bm_types.h"

/*==================[external functions declaration]=========================*/

/**
 * @brief Initializes USART2 at 115200 Baud
 * @note USART2 can be read from USB with a serial port terminal
 */
void UartInit(void);

/**
 * @brief Sends n characters to USART2
 *
 * @param[in] 	txt 	Pointer to array of characters
 * @param[in] 	n 		Number of characters to send
 */
void UartSendBlocking(char *txt,uint32_t n);

/**
 * @brief  Reads n characters from USART2
 *
 * @param[in] 	txt 	Pointer to array of characters
 * @param[in] 	n 		Number of characters to read
 */
void UartReadBlocking(char *txt,uint32_t n);

/**
 * @brief Sends array of char that end with '\0'
 *
 * @param[in] 	txt 	Pointer to array of characters
 */
void UartPrint(char *txt);

/**
 * @brief Converts a decimal number to a ASCII representation
 *
 * @param[in]	value	Unsigned integer (max 32 bits word)
 * @param[out]	dec		Pointer to array of char (decimal format). Size: 11 (10 digits + '\0')
 */
void Uint32ToDec(uint32_t value, char * dec);

/**
 * @brief Converts a hexadecimal number to a ASCII representation
 *
 * @param[in]	value	Unsigned integer (max 32 bits word)
 * @param[out] 	hex 	Pointer to array of char (hexadecimal format). Size: 9 (8 digits + '\0')
 */
void Uint32ToHex(uint32_t value, char * hex);

/**
 * @brief Converts a binary number to a ASCII representation
 *
 * @param[in]	value	Unsigned integer (max 32 bits word)
 * @param[out] 	bin		Pointer to array of char (binary format). Size: 33 (32 digits + '\0')
 */
void Uint32ToBin(uint32_t value, char * bin);

/**
 * @brief Print through USART2 a message and a value in decimal format
 *
 * @param[in] msg 	Pointer to message ended with '\0'
 * @param[in] value	Unsigned integer (max 32 bits word)
 */
void UartPrintMsgAndDec(char * msg,uint32_t value);

/**
 * @brief Print through USART2 a message and a value in hexadecimal format
 *
 * @param[in] msg 	Pointer to message ended with '\0'
 * @param[in] value	Unsigned integer (max 32 bits word)
 */
void UartPrintMsgAndHex(char * msg,uint32_t value);

/**
 * @brief Print through USART2 a message and a value in binary format
 *
 * @param[in] msg 	Pointer to message ended with '\0'
 * @param[in] value	Unsigned integer (max 32 bits word)
 */
void UartPrintMsgAndBin(char * msg,uint32_t value);

#endif /* MODULES_LPC4337_M4_DRIVERS_BM_INC_UART_DEBUG_H_ */

/** @} doxygen end group definition */
