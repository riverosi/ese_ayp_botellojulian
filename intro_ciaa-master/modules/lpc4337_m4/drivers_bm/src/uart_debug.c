/** @addtogroup debug_uart Debug UART
 *  @{
 */

/*==================[inclusions]=============================================*/

#include "uart_debug.h"
#include "chip.h"
#include "string.h"

/*==================[internal data definition]===============================*/

static const char hex_lut [16] = {
		'0', '1', '2', '3',
		'4', '5', '6', '7',
		'8', '9', 'A', 'B',
		'C', 'D', 'E', 'F'
};

/*==================[internal functions declaration]==========================*/

static uint8_t DecimalDigits(uint32_t value, uint8_t * digits);

/*==================[external functions definition]==========================*/

void UartInit(void){
	//MUX UART
	Chip_SCU_PinMux(7, 1, MD_PDN, FUNC6);
	Chip_SCU_PinMux(7, 2, MD_PLN|MD_EZI|MD_ZI, FUNC6);
	//UART CONFIG
	Chip_UART_Init(LPC_USART2);
    Chip_UART_ConfigData(LPC_USART2, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS);
    Chip_UART_SetBaud(LPC_USART2, 115200);
    Chip_UART_SetupFIFOS(LPC_USART2, (UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS | UART_FCR_TRG_LEV3));
    //UART ENABLE
    Chip_UART_TXEnable(LPC_USART2);
}

void UartSendBlocking(char *txt,uint32_t n){
	Chip_UART_SendBlocking ( LPC_USART2 , txt , n);
}


void UartReadBlocking(char *txt,uint32_t n){
	Chip_UART_ReadBlocking ( LPC_USART2 , txt , n);
}

void UartPrint(char *txt){
	Chip_UART_SendBlocking ( LPC_USART2 , txt , strlen(txt));
}

void Uint32ToDec(uint32_t value, char * dec){
	uint8_t aux_buf[10];
	uint8_t N=DecimalDigits(value,aux_buf);
	for(uint8_t i=0; i<N;i++)
		dec[i]=aux_buf[i]+'0';
	dec[N]='\0';
}

void Uint32ToHex (uint32_t value, char * hex){

	for(uint8_t i=0;i<4;i++){
		uint8_t b= (uint8_t) (value>>(i*8)) & 0xFF;
		uint8_t b_high= (b>>4) & 0x0F;
		uint8_t b_low= 		 b & 0x0F;
		hex[7-2*i]=hex_lut[b_low];  //saved inverted
		hex[6-2*i]=hex_lut[b_high];  //saved inverted
	}

	// Initially the most significant hex position(mshex_pos)
	int8_t mshex_pos=0;
	// Then check in reverse(to right) a 1. Stop when is found. msb_pos has the position.
	while(hex[mshex_pos]=='0'&&mshex_pos<7){
		mshex_pos++;
	}
	// so shift the hex to left
	uint8_t i=0;
	for(i=mshex_pos;i<8;i++){
		hex[i-mshex_pos]=hex[i];
	}
	// then '\0' at the end
	hex[8-mshex_pos]='\0';

}

void Uint32ToBin(uint32_t ui32,char * bin){
	for(uint8_t i=0;i<32;i++){
		bin[31-i]=((ui32>>i)&0x01) + '0'; //saved inverted
	}
	// Initially the most significant bit position(msb_pos)
	int8_t msb_pos=0;
	// Then check in reverse(to right) a 1. Stop when is found. msb_pos has the position.
	while(bin[msb_pos]=='0'&&msb_pos<31){
		msb_pos++;
	}
	// so shift the binary to left
	uint8_t i=0;
	for(i=msb_pos;i<32;i++){
		bin[i-msb_pos]=bin[i];
	}
	// then '\0' at the end
	bin[32-msb_pos]='\0';
}

void UartPrintMsgAndDec(char * msg,uint32_t value){
	char aux[10+1]; // 10 dec + '\0'
	UartPrint(msg);
	Uint32ToDec(value,aux);
	UartPrint(aux);
	UartPrint("\n");
}

void UartPrintMsgAndHex(char * msg,uint32_t value){
	char aux[8+1]; // 8 hex + '\0'
	UartPrint(msg);
	Uint32ToHex(value,aux);
	UartPrint(aux);
	UartPrint("\n");
}

void UartPrintMsgAndBin(char * msg,uint32_t value){
	char aux[32+1];  // 32 bin + '\0'
	UartPrint(msg);
	Uint32ToBin(value,aux);
	UartPrint(aux);
	UartPrint("\n");
}

/*==================[internal functions definition]==========================*/

static uint8_t DecimalDigits(uint32_t value, uint8_t * digits){

	uint8_t cant=1;
	uint8_t aux[10];
	aux[cant-1]=value%10;
	while(value>=10){
		cant++;
		value/=10;
		aux[cant-1]=value%10;
	}
	for(uint8_t i=0;i<cant;i++){
		digits[cant-1-i]=aux[i];
	}
	return cant;
}

/** @} doxygen end group definition */
